import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection = this.db.collection('students');

  addStudent(email:string,name:string,average:number,psychometric:number,result:string){
    const student = {email:email, name:name, average:average, psychometric:psychometric, result:result};
    this.db.collection(`students`).add(student);
  }

  
  getStudents(){
    this.studentCollection = this.db.collection(`students`);
    return this.studentCollection.snapshotChanges();
  }

  deleteStudents(studentId:string){
    this.db.doc(`students/${studentId}`).delete();
  }

  
  constructor(private db:AngularFirestore) { }
}
