import { Student } from './../interfaces/student';
import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId;
  students$;
  students=[];  
  displayedColumns: string[] = ['name', 'email', 'average', 'psychometric','predict','delete'];

  delete(studentId:string){
    this.studentsService.deleteStudents(studentId);
  }

  constructor(private studentsService:StudentsService, public auth:AuthService) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.students$ = this.studentsService.getStudents();
        this.students$.subscribe(
          docs => {
            this.students = [];
            for (let document of docs){
              const student:any = document.payload.doc.data();
              student.id = document.payload.doc.id;
              this.students.push(student);
            }
          }
        )
      }
    )
  }
  

}
