import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LambdaService {

  private url = "https://gegawy6chj.execute-api.us-east-1.amazonaws.com/exam";

  predict(average:number, psychometric:number,isPay:number ){
    let json = {
      "data": {
          "average": average,
          "psychometric": psychometric,
          "isPay": isPay

        }
    }
    let body = JSON.stringify(json);
    console.log(body);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body; 
      })
    )
  }

  constructor(private http:HttpClient) { }

}
