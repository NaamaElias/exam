import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { Student } from './../interfaces/student';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LambdaService } from '../lambda.service';


@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  name:string;
  average:number;
  psychometric:number;
  isPay:Boolean;
  @Output() update = new EventEmitter<Student>();
  @Output() closeForm = new EventEmitter<null>();
  isError:boolean = false;
  result:string;
  selectedValue;
  selectedValue2;
  default;
  email;

  // isChecked;
  // isCheckedName;
  // checkboxData = ['pay'];
  // onChange(e){       
  //   this.isChecked = !this.isChecked;
  //   this.isCheckedName = e.target.name;
  // }

  choises: any[] = [
    {value: 'pay', viewValue: 'yes'},
    {value: 'not pay', viewValue: 'No'}
  ];

  choises2: any[] = [
    {value: '1', viewValue: 'Did not drop out of school'},
    {value: '2', viewValue: 'Falls out'}
  ];

  predict(){
    if(this.selectedValue == 'pay'){
    var pay = 1;
    this.lmbdaService.predict(this.average,this.psychometric,pay).subscribe(
      res => {console.log(res);
        if(res>=0.5){
          this.result = "Did not drop out of school";
        }else{
          this.result = "Falls out";
        }
        this.default = this.result;
      // alert(this.result);
      //  alert(this.default);
    }
    );     
    }else{
    var pay = 0;
    this.lmbdaService.predict(this.average,this.psychometric,pay).subscribe(
      res => {console.log(res);
        if(res>=0.5){
          this.result = "Did not drop out of school";
        }else{
          this.result = "Falls out";
        }
        this.default = this.result;
    }
    );     
    }
  }

add(){
  this.studentService.addStudent(this.email, this.name, this.average, this.psychometric, this.result);
  this.router.navigate(['/students']);
}  

  constructor(private lmbdaService:LambdaService, private studentService:StudentsService, public auth:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
        this.email = user.email;
      }
    )
  }

}
